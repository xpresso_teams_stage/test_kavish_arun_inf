## Default Makefile generated for the  project : sample
##
export PROJECT_VERSION
export PROJECT_NAME := test_kavish_arun_inf

default: debug

# Clean the projects
clean:
	$(MAKE) -C ${SUBDIR} clean

clobber:
	$(MAKE) -C ${SUBDIR} clobber

# Perform test
lint:
	$(MAKE) -C ${SUBDIR} lint

unittest:
	$(MAKE) -C ${SUBDIR} unittest

apitest:
	$(MAKE) -C ${SUBDIR} apitest

systemtest:
	$(MAKE) -C ${SUBDIR} systemtest

test-all:
	$(MAKE) -C ${SUBDIR} test-all

# Build
prepare:
	$(MAKE) -C ${SUBDIR} prepare

debug:
	$(MAKE) -C ${SUBDIR} debug

release:
	$(MAKE) -C ${SUBDIR} release

module:
	$(MAKE) -C ${SUBDIR} module

all:
	$(MAKE) -C ${SUBDIR} all

build:
	$(MAKE) -C ${SUBDIR} build

dist:
	$(MAKE) -C ${SUBDIR} dist

# Train
train:
	$(MAKE) -C ${SUBDIR} train

# Deployment
dockerpush:
	$(MAKE) -C ${SUBDIR} dockerpush

deploy-local: run
	$(MAKE) -C ${SUBDIR} deploy-local

deploy-debug:
	$(MAKE) -C ${SUBDIR} deploy-debug

deploy-release:
	$(MAKE) -C ${SUBDIR} deploy-release

run:
	$(MAKE) -C ${SUBDIR} run

local-env:
	/bin/bash xpresso_ai/scripts/python/local_development/start-env.sh --command start

local-env-win:
	/bin/bash xpresso_ai/scripts/python/local_development/start-env.bat --command start --command start

# utils
doc:
	$(MAKE) -C ${SUBDIR} doc

version:
	$(MAKE) -C ${SUBDIR} version

.PHONY:
	clean all debug debug-local release deploy deploy-local run run-local
